-- phpMyAdmin SQL Dump
-- version 5.0.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-02-2021 a las 00:41:30
-- Versión del servidor: 10.4.17-MariaDB
-- Versión de PHP: 7.3.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `dbzinc`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `administrador`
--

CREATE TABLE `administrador` (
  `idAdministrador` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `estado` int(11) NOT NULL,
  `codigoActivacion` int(11) NOT NULL,
  `foto` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `administrador`
--

INSERT INTO `administrador` (`idAdministrador`, `nombre`, `correo`, `clave`, `estado`, `codigoActivacion`, `foto`) VALUES
(17, 'Alejandro Acosta', 'alejandro@hotmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, 'Vista/Img/Users/1614118279.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE `cliente` (
  `idCliente` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `estado` int(11) NOT NULL,
  `codigoActivacion` int(11) NOT NULL,
  `foto` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`idCliente`, `nombre`, `correo`, `clave`, `estado`, `codigoActivacion`, `foto`) VALUES
(43, 'Jorge Riverdale', 'Jorge@hotmail.com', '202cb962ac59075b964b07152d234b70', 1, 1, 'Vista/Img/Users/1614113234.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `idFactura` int(11) NOT NULL,
  `fecha` datetime NOT NULL,
  `idCliente` int(11) NOT NULL,
  `valor` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`idFactura`, `fecha`, `idCliente`, `valor`) VALUES
(140, '2020-09-12 20:14:17', 43, 42000),
(141, '2021-02-23 16:06:10', 43, 1500000),
(142, '2021-02-23 16:28:14', 43, 3000000),
(143, '2021-02-23 16:29:45', 43, 5160000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `facturaproducto`
--

CREATE TABLE `facturaproducto` (
  `idfacturaProducto` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `idFactura` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `facturaproducto`
--

INSERT INTO `facturaproducto` (`idfacturaProducto`, `idProducto`, `idFactura`, `cantidad`, `precio`) VALUES
(154, 1, 140, 5, 3000),
(155, 2, 140, 5, 3000),
(156, 3, 140, 5, 2400),
(157, 2, 141, 1, 1500000),
(158, 1, 142, 10, 300000),
(159, 5, 143, 6, 860000);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `log`
--

CREATE TABLE `log` (
  `idLog` int(11) NOT NULL,
  `accion` varchar(50) NOT NULL,
  `datos` varchar(200) NOT NULL,
  `fecha` date NOT NULL,
  `hora` time NOT NULL,
  `actor` varchar(50) NOT NULL,
  `idUsuario` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `log`
--

INSERT INTO `log` (`idLog`, `accion`, `datos`, `fecha`, `hora`, `actor`, `idUsuario`) VALUES
(535, 'Inicio de Sesion', '', '2020-09-12', '20:10:43', 'Cliente', 48),
(536, 'Inicio de Sesion', '', '2020-09-12', '20:13:04', 'Proveedor', 33),
(537, 'Proveer Producto', 'id:5-cantidad:100', '2020-09-12', '20:13:19', 'Proveedor', 33),
(538, 'Inicio de Sesion', '', '2020-09-12', '20:13:51', 'Cliente', 43),
(539, 'Compra', 'factura:140', '2020-09-12', '20:14:17', 'Cliente', 43),
(540, 'Inicio de Sesion', '', '2020-09-12', '20:18:51', 'Administrador', 17),
(541, 'Inicio de Sesion', '', '2020-09-12', '20:19:57', 'Proveedor', 33),
(542, 'Inicio de Sesion', '', '2020-09-12', '20:20:43', 'Cliente', 43),
(543, 'Inicio de Sesion', '', '2021-02-10', '23:22:42', 'Administrador', 17),
(544, 'Inicio de Sesion', '', '2021-02-10', '23:54:04', 'Administrador', 17),
(545, 'Inicio de Sesion', '', '2021-02-11', '00:14:31', 'Cliente', 54),
(546, 'Inicio de Sesion', '', '2021-02-21', '01:52:00', 'Administrador', 17),
(547, 'Actualizar Información', 'Actor:Administrador-Nombre:Alejandro Acosta-Correo:alejandro@hotmail.com-id:17', '2021-02-21', '01:59:00', 'Administrador', 17),
(548, 'Actualizar Información', 'Actor:Administrador-Nombre:Alejandro Acosta-Correo:alejandro@hotmail.com-id:17', '2021-02-21', '01:59:25', 'Administrador', 17),
(549, 'Actualizar Información', 'Actor:Administrador-Nombre:Alejandro Acosta-Correo:alejandro@hotmail.com-id:17', '2021-02-21', '01:59:47', 'Administrador', 17),
(550, 'Actualizar Información', 'Actor:Administrador-Nombre:Alejandro Acosta-Correo:alejandro@hotmail.com-id:17', '2021-02-21', '02:01:18', 'Administrador', 17),
(551, 'Inicio de Sesion', '', '2021-02-21', '02:17:39', 'Administrador', 17),
(552, 'Editar Producto', 'id:1 -Nombre:Naranja-Cantidad:77-Precio:3000', '2021-02-21', '02:26:41', 'Administrador', 17),
(553, 'Cambio Estado Producto', 'idProducto:1@Estado:0', '2021-02-21', '02:33:09', 'Administrador', 17),
(554, 'Cambio Estado Producto', 'idProducto:5@Estado:0', '2021-02-21', '02:33:14', 'Administrador', 17),
(555, 'Inicio de Sesion', '', '2021-02-21', '02:33:33', 'Cliente', 43),
(556, 'Inicio de Sesion', '', '2021-02-21', '02:34:49', 'Administrador', 17),
(557, 'Inicio de Sesion', '', '2021-02-21', '23:23:27', 'Administrador', 17),
(558, 'Inicio de Sesion', '', '2021-02-21', '23:23:30', 'Administrador', 17),
(559, 'Inicio de Sesion', '', '2021-02-21', '23:45:32', 'Administrador', 17),
(560, 'Inicio de Sesion', '', '2021-02-22', '15:14:08', 'Administrador', 17),
(561, 'Inicio de Sesion', '', '2021-02-22', '17:57:01', 'Cliente', 43),
(562, 'Inicio de Sesion', '', '2021-02-23', '15:23:04', 'Cliente', 43),
(563, 'Inicio de Sesion', '', '2021-02-23', '15:42:04', 'Administrador', 17),
(564, 'Inicio de Sesion', '', '2021-02-23', '15:44:09', 'Cliente', 43),
(565, 'Actualizar Información', 'Actor:Cliente-Nombre:Kevin Lopez-Correo:kevin@hotmail.com-id:43', '2021-02-23', '15:47:15', 'Cliente', 43),
(566, 'Inicio de Sesion', '', '2021-02-23', '15:51:55', 'Administrador', 17),
(567, 'Crear Producto', 'id:52', '2021-02-23', '15:53:07', 'Administrador', 17),
(568, 'Cambio Estado Producto', 'idProducto:2@Estado:1', '2021-02-23', '15:53:44', 'Administrador', 17),
(569, 'Cambio Estado Producto', 'idProducto:3@Estado:1', '2021-02-23', '15:53:48', 'Administrador', 17),
(570, 'Cambio Estado Producto', 'idProducto:5@Estado:1', '2021-02-23', '15:53:55', 'Administrador', 17),
(571, 'Cambio Estado Producto', 'idProducto:52@Estado:1', '2021-02-23', '15:53:59', 'Administrador', 17),
(572, 'Cambio Estado Producto', 'idProducto:52@Estado:0', '2021-02-23', '15:54:20', 'Administrador', 17),
(573, 'Editar Producto', 'id:2 -Nombre:Fresas-Cantidad:84-Precio:3000', '2021-02-23', '15:56:32', 'Administrador', 17),
(574, 'Editar Producto', 'id:3 -Nombre:Manzanas-Cantidad:92-Precio:2400', '2021-02-23', '15:57:29', 'Administrador', 17),
(575, 'Editar Producto', 'id:5 -Nombre:Bananos-Cantidad:110-Precio:1400', '2021-02-23', '16:00:02', 'Administrador', 17),
(576, 'Editar Producto', 'id:1 -Nombre:Scooter Roja-Cantidad:77-Precio:300000', '2021-02-23', '16:01:07', 'Administrador', 17),
(577, 'Crear Producto', 'id:53', '2021-02-23', '16:02:10', 'Administrador', 17),
(578, 'Crear Producto', 'id:54', '2021-02-23', '16:03:18', 'Administrador', 17),
(579, 'Crear Producto', 'id:55', '2021-02-23', '16:03:45', 'Administrador', 17),
(580, 'Crear Producto', 'id:56', '2021-02-23', '16:04:10', 'Administrador', 17),
(581, 'Compra', 'factura:141', '2021-02-23', '16:06:10', 'Cliente', 43),
(582, 'Compra', 'factura:142', '2021-02-23', '16:28:15', 'Cliente', 43),
(583, 'Cambio Estado Producto', 'idProducto:1@Estado:0', '2021-02-23', '16:28:41', 'Administrador', 17),
(584, 'Compra', 'factura:143', '2021-02-23', '16:29:45', 'Cliente', 43),
(585, 'Cambio Estado Producto', 'idProducto:5@Estado:0', '2021-02-23', '16:30:35', 'Administrador', 17),
(586, 'Inicio de Sesion', '', '2021-02-23', '16:34:18', 'Proveedor', 33),
(587, 'Inicio de Sesion', '', '2021-02-23', '16:34:53', 'Administrador', 17),
(588, 'Cambio Estado Producto', 'idProducto:2@Estado:0', '2021-02-23', '16:35:06', 'Administrador', 17),
(589, 'Cambio Estado Producto', 'idProducto:5@Estado:1', '2021-02-23', '16:35:10', 'Administrador', 17),
(590, 'Cambio Estado Producto', 'idProducto:52@Estado:1', '2021-02-23', '16:35:18', 'Administrador', 17),
(591, 'Inicio de Sesion', '', '2021-02-23', '16:44:22', 'Administrador', 17),
(592, 'Inicio de Sesion', '', '2021-02-23', '17:02:58', 'Administrador', 17),
(593, 'Actualizar Información', 'Actor:Administrador-Nombre:Alejandro Acosta-Correo:alejandro@hotmail.com-id:17', '2021-02-23', '17:11:19', 'Administrador', 17),
(594, 'Inicio de Sesion', '', '2021-02-23', '17:18:58', 'Cliente', 43),
(595, 'Inicio de Sesion', '', '2021-02-23', '17:19:57', 'Administrador', 17),
(596, 'Inicio de Sesion', '', '2021-02-23', '17:22:57', 'Cliente', 43),
(597, 'Inicio de Sesion', '', '2021-02-23', '17:50:41', 'Proveedor', 33),
(598, 'Inicio de Sesion', '', '2021-02-23', '18:08:11', 'Proveedor', 33),
(599, 'Inicio de Sesion', '', '2021-02-23', '18:08:11', 'Proveedor', 33),
(600, 'Actualizar Información', 'Actor:Proveedor-Nombre:Jose-Correo:Jose@Hotmail.com-id:33', '2021-02-23', '18:16:51', 'Proveedor', 33),
(601, 'Inicio de Sesion', '', '2021-02-23', '18:17:56', 'Administrador', 17),
(602, 'Inicio de Sesion', '', '2021-02-23', '18:37:59', 'Administrador', 17);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `producto`
--

CREATE TABLE `producto` (
  `idProducto` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(11) NOT NULL,
  `imagen` varchar(50) NOT NULL,
  `estado` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `producto`
--

INSERT INTO `producto` (`idProducto`, `nombre`, `cantidad`, `precio`, `imagen`, `estado`) VALUES
(1, 'CARBONREVO DUALTRON 3MM DECK COVER', 67, 300000, 'Vista/Img/imgProductos/1614114067.jpg', 1),
(2, 'Dualtron Eagle Pro', 22, 1500000, 'Vista/Img/imgProductos/1614113792.jpg', 1),
(3, 'Dualtron X2', 30, 2400000, 'Vista/Img/imgProductos/1614113849.jpg', 0),
(5, 'Thunder', -3, 860000, 'Vista/Img/imgProductos/1614114002.jpg', 0),
(52, 'Compact Electric Scooter', 4, 2569000, 'Vista/Img/imgProductos/1614113587.jpeg', 0),
(53, 'CARBONREVO DUALTRON ALUMINUM STEM', 56, 345999, 'Vista/Img/imgProductos/1614114129.jpeg', 1),
(54, 'CARBONREVO DUALTRON BOX BRACKET', 56, 305999, 'Vista/Img/imgProductos/1614114198.jpeg', 1),
(55, 'CARBONREVO DUALTRON CARBON FIBRE REAR MUDGUARD', 3, 205999, 'Vista/Img/imgProductos/1614114225.jpeg', 1),
(56, 'CARBONREVO DUALTRON DECK LINER', 45, 700999, 'Vista/Img/imgProductos/1614114250.jpeg', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedor`
--

CREATE TABLE `proveedor` (
  `idProveedor` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `clave` varchar(50) NOT NULL,
  `estado` int(11) NOT NULL,
  `codigoActivacion` int(11) NOT NULL,
  `foto` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedor`
--

INSERT INTO `proveedor` (`idProveedor`, `nombre`, `correo`, `clave`, `estado`, `codigoActivacion`, `foto`) VALUES
(33, 'Jose', 'Jose@Hotmail.com', 'caf1a3dfb505ffed0d024130f58c5cfa', 1, 1, 'Vista/Img/Users/1614122211.jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proveedorproducto`
--

CREATE TABLE `proveedorproducto` (
  `idProveedorProducto` int(11) NOT NULL,
  `idProducto` int(11) NOT NULL,
  `idProveedor` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `precio` int(50) NOT NULL,
  `fecha` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `proveedorproducto`
--

INSERT INTO `proveedorproducto` (`idProveedorProducto`, `idProducto`, `idProveedor`, `cantidad`, `precio`, `fecha`) VALUES
(25, 5, 33, 10, 700, '2020-09-12 20:13:19');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `administrador`
--
ALTER TABLE `administrador`
  ADD PRIMARY KEY (`idAdministrador`);

--
-- Indices de la tabla `cliente`
--
ALTER TABLE `cliente`
  ADD PRIMARY KEY (`idCliente`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`idFactura`),
  ADD KEY `factura_ibfk_1` (`idCliente`);

--
-- Indices de la tabla `facturaproducto`
--
ALTER TABLE `facturaproducto`
  ADD PRIMARY KEY (`idfacturaProducto`),
  ADD KEY `idProducto` (`idProducto`),
  ADD KEY `idFactura` (`idFactura`);

--
-- Indices de la tabla `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`idLog`);

--
-- Indices de la tabla `producto`
--
ALTER TABLE `producto`
  ADD PRIMARY KEY (`idProducto`);

--
-- Indices de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  ADD PRIMARY KEY (`idProveedor`);

--
-- Indices de la tabla `proveedorproducto`
--
ALTER TABLE `proveedorproducto`
  ADD PRIMARY KEY (`idProveedorProducto`),
  ADD KEY `idProducto` (`idProducto`),
  ADD KEY `idProveedor` (`idProveedor`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `administrador`
--
ALTER TABLE `administrador`
  MODIFY `idAdministrador` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT de la tabla `cliente`
--
ALTER TABLE `cliente`
  MODIFY `idCliente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=70;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `idFactura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=144;

--
-- AUTO_INCREMENT de la tabla `facturaproducto`
--
ALTER TABLE `facturaproducto`
  MODIFY `idfacturaProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=160;

--
-- AUTO_INCREMENT de la tabla `log`
--
ALTER TABLE `log`
  MODIFY `idLog` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=603;

--
-- AUTO_INCREMENT de la tabla `producto`
--
ALTER TABLE `producto`
  MODIFY `idProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT de la tabla `proveedor`
--
ALTER TABLE `proveedor`
  MODIFY `idProveedor` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT de la tabla `proveedorproducto`
--
ALTER TABLE `proveedorproducto`
  MODIFY `idProveedorProducto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `factura`
--
ALTER TABLE `factura`
  ADD CONSTRAINT `factura_ibfk_1` FOREIGN KEY (`idCliente`) REFERENCES `cliente` (`idCliente`);

--
-- Filtros para la tabla `facturaproducto`
--
ALTER TABLE `facturaproducto`
  ADD CONSTRAINT `facturaproducto_ibfk_1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`),
  ADD CONSTRAINT `facturaproducto_ibfk_2` FOREIGN KEY (`idFactura`) REFERENCES `factura` (`idFactura`);

--
-- Filtros para la tabla `proveedorproducto`
--
ALTER TABLE `proveedorproducto`
  ADD CONSTRAINT `proveedorproducto_ibfk_1` FOREIGN KEY (`idProducto`) REFERENCES `producto` (`idProducto`),
  ADD CONSTRAINT `proveedorproducto_ibfk_2` FOREIGN KEY (`idProveedor`) REFERENCES `proveedor` (`idProveedor`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
