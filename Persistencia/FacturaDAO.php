<?php
class FacturaDAO{
    private $idFactura;
    private $fecha;
    private $idCliente;
    private $valor;


    public function FacturaDAO($idFactura = "", $fecha = "", $idCliente = "", $valor = 0)
    {
        $this->idFactura = $idFactura;
        $this->fecha = $fecha;
        $this->idCliente = $idCliente;
        $this->valor = $valor;
    }

    public function setIdFactura($idFactura)
    {
        $this->idFactura = $idFactura;
    }
    public function setValor($valor)
    {
        $this->valor = $valor;
    }

    public function traerInfo(){
        return "select idFactura, fecha, idCliente, valor
                from factura
                where idFactura = '".$this -> idFactura."'";
    }

    public function crearFactura(){
        return "insert into factura (fecha,idCliente,valor) values('".$this -> fecha."','".$this -> idCliente."','".$this -> valor."')";
    }

    public function facturas(){
        return "select * from factura";
    }

    public function actualizarValor(){
        return "update factura set valor = '".$this -> valor."' where idFactura = '".$this -> idFactura."'";
    }

    public function cantidadPaginasFiltro($filtro){
        return "select count(idFactura) 
                from factura
                where idFactura like '%".$filtro."%' or fecha like '%".$filtro."%' or idCliente like '".$filtro."%' or valor like '".$filtro."%'";
    }

    public function listarFiltro($filtro,$cantidad,$pagina){
        return "select idFactura, fecha, idCliente, valor
                from factura
                where idFactura like '%".$filtro."%' or fecha like '%".$filtro."%' or idCliente like '".$filtro."%' or valor like '".$filtro."%'
                order by idFactura DESC
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function cantidadPaginasFiltroCliente($filtro){
        return "select count(idFactura) 
                from factura
                where (idFactura like '%".$filtro."%' or fecha like '%".$filtro."%' or valor like '".$filtro."%')
                and idCliente = '".$this -> idCliente."'";
    }

    public function listarFiltroCliente($filtro,$cantidad,$pagina){
        return "select idFactura, fecha, idCliente, valor
                from factura
                where (idFactura like '%".$filtro."%' or fecha like '%".$filtro."%' or valor like '".$filtro."%')
                and idCliente = '".$this -> idCliente."'
                order by idFactura DESC
                limit " . (($pagina-1) * $cantidad) . ", " . $cantidad;
    }

    public function consultarsemana(){

       
        $fecha_actual = date("y-m-d");
        
        $fecha_2= date("y-m-d",strtotime($fecha_actual."- 1 days")); 
        $fecha_3= date("y-m-d",strtotime($fecha_actual."- 2 days")); 
        $fecha_4= date("y-m-d",strtotime($fecha_actual."- 3 days")); 
        $fecha_5= date("y-m-d",strtotime($fecha_actual."- 4 days")); 
        $fecha_6= date("y-m-d",strtotime($fecha_actual."- 5 days")); 
        $fecha_7= date("y-m-d",strtotime($fecha_actual."- 6 days")); 
        $fecha_8= date("y-m-d",strtotime($fecha_actual."- 7 days")); 

        return "SELECT fecha, SUM(valor)
FROM `factura` 
WHERE fecha<='$fecha_actual'and fecha>='$fecha_2' 
UNION 
SELECT fecha,SUM(valor)
FROM `factura` 
WHERE fecha<='$fecha_2'and fecha>'$fecha_3'
UNION
SELECT fecha, SUM(valor)
FROM `factura` 
WHERE fecha<='$fecha_3'and fecha>'$fecha_4'
UNION
SELECT fecha,SUM(valor)
FROM `factura` 
WHERE fecha<='$fecha_4'and fecha>'$fecha_5'
UNION
SELECT fecha, SUM(valor)
FROM `factura` 
WHERE fecha<='$fecha_5'and fecha>'$fecha_6'
UNION
SELECT fecha,SUM(valor)
FROM `factura` 
WHERE fecha<='$fecha_6'and fecha>'$fecha_7'  
UNION
SELECT fecha,SUM(valor)
FROM `factura` 
WHERE fecha<='$fecha_7'and fecha>'$fecha_8'   ";
        

        
    }

    public function consultarmes(){

       
        $mes_actual = date("y-m-d");
        
        $mes_2= date("y-m-d",strtotime($mes_actual."- 7 days")); 
        $mes_3= date("y-m-d",strtotime($mes_actual."- 14 days")); 
        $mes_4= date("y-m-d",strtotime($mes_actual."- 21 days")); 
        $mes_5= date("y-m-d",strtotime($mes_actual."- 31 days")); 
       
        
        return "SELECT fecha, SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_actual'and fecha>'$mes_2' 
UNION 
SELECT fecha,SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_2'and fecha>'$mes_3'
UNION
SELECT fecha, SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_3'and fecha>'$mes_4'
UNION
SELECT fecha, SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_4'and fecha>'$mes_5'";
        

        
    }
    public function consultaraño(){

       
        $mes_actual = date("y-m-d");
        
        $mes_2= date("y-m-d",strtotime($mes_actual."- 1 month")); 
        $mes_3= date("y-m-d",strtotime($mes_actual."- 2 month")); 
        $mes_4= date("y-m-d",strtotime($mes_actual."- 3 month")); 
        $mes_5= date("y-m-d",strtotime($mes_actual."- 4 month")); 
        $mes_6= date("y-m-d",strtotime($mes_actual."- 5 month")); 
        $mes_7= date("y-m-d",strtotime($mes_actual."- 6 month")); 
        $mes_8= date("y-m-d",strtotime($mes_actual."- 7 month")); 
        
        return "SELECT fecha, SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_actual'and fecha>'$mes_2' 
UNION 
SELECT fecha,SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_2'and fecha>'$mes_3'
UNION
SELECT fecha, SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_3'and fecha>'$mes_4'
UNION
SELECT fecha,SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_4'and fecha>'$mes_5'
UNION
SELECT fecha, SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_5'and fecha>'$mes_6'
UNION
SELECT fecha,SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_6'and fecha>'$mes_7'  
UNION
SELECT fecha,SUM(valor)
FROM `factura` 
WHERE fecha<='$mes_7'and fecha>'$mes_8'   ";
        

        
    }
    public function consultarporscooter(){

                
        
       
            return "SELECT l.nombre, SUM(a.cantidad) as cantidad
            from facturaproducto a join producto l on (a.idProducto = l.idProducto)
             group by l.nombre";
        

        
    }
    public function listar(){
    return "SELECT idFactura, fecha, idCliente, valor
    FROM factura 
    WHERE idCliente = '".$this -> idCliente."'";
    }

}
