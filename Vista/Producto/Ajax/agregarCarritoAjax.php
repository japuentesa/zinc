<?php

$carrito;

$carrito = $_SESSION["carrito"]; 
$carrito = unserialize($carrito); 

$productoBodega = new Producto($_POST["idProducto"]);
$productoBodega->traerInfo();
$band = false;

if ($productoBodega->getCantidad() >= $_POST["Cantidad"]) {
    for ($i = 0; $i < count($carrito->getArrayProductos()); $i++) {
        
        if ($carrito->getArrayProductos()[$i]->getidProducto() == $_POST["idProducto"]) {
            $band = true;
            $carrito->getArrayProductos()[$i]->setCantidad($carrito->getArrayProductos()[$i]->getCantidad() + $_POST["Cantidad"]);
        }
    }
    if ($band == false) {
        $producto = new Producto($_POST["idProducto"], $_POST["Nombre"], $_POST["Cantidad"], $_POST["Precio"], $_POST["Imagen"]);
        $carrito->agregarProducto($producto);
        $band=true;
    }
}

$_SESSION["carrito"] = serialize($carrito);

$array = array(
    "estado" => $band,
    "cantidad" => count($carrito->getArrayProductos()),
);
$objJSON = json_encode($array);
echo $objJSON;
