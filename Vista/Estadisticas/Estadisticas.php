<?php

$factura = new Factura();
$Cantidadporscooter = $factura->consultarporscooter();
$datos = "['Scooter', 'Ventas'], ";
foreach ($Cantidadporscooter as $l) {
    $datos .= "['" . $l[0]  . " ', " . $l[1] . "], ";
}
$datos = substr($datos, 0, strlen($datos) - 2);

?>

<div class="container" style="background-color: #9c9c9c;">
    <div class="card mt-6">
        <div class="card-header text-white bg-dark text-center">
            <h4>Estadisticas</h4>
        </div>
        <div class="card mt-4">
        </div>
        <div id="chart_div">



                    </div>
        <div class="d-flex text-center m-2 shadow-sm p-3 e rounded">
        
            <div class="col-xl-7 col-lg-7 col-md-7 col-sm-7 col-12">

                <div class="row mt-4">
                    <div class="col-lg-6"></div>
                    <div class="col-lg-6 text-center">
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <label class="input-group-text">Filtro de Ingresos</label>
                                </div>
                                <select class="custom-select" id="edition">
                                    <option value="-1">Seleccione:</option>
                                    <option value="3">Semanal</option>
                                    <option value="2">Mensual</option>
                                    <option value="1">Anual</option>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
                <div id="result">

                    
                </div>
            </div>
            




            <head>
                <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
                <script type="text/javascript">
                    google.charts.load('current', {
                        packages: ['corechart', 'bar']
                    });
                    google.charts.setOnLoadCallback(drawBasic);

                    function drawBasic() {

                        var data = google.visualization.arrayToDataTable([
                            <?php echo $datos ?>
                        ]);

                        var options = {
                            title: 'Ventas scooter o repuesto',
                            chartArea: {
                                width: '30%'
                            },
                            hAxis: {
                                title: 'Total ventas',
                                minValue: 0
                            },
                            vAxis: {
                                title: 'Scooter'
                            }
                        };

                        var chart = new google.visualization.BarChart(document.getElementById('chart_div'));

                        chart.draw(data, options);
                    }
                </script>
                <div id="barchart_values" style="width: 900px; height: 900px;"></div>
            </head>

            <body>
                <div id="top_x_div" style="width: 1000px; height: 1000px;"></div>
            </body>




        </div>




    </div>
</div>
<div class="row mt-4">
</div>
</div>
<script>
    $(document).ready(function() {
        $("#edition").change(function() {
            if ($("#edition").val() != -1) {
                $("#result").html("<div class='text-center'><img src='Loadingg.gif'></div>");
                var edition = $("#edition").val();
                var path = "indexAjax.php?pid=<?php echo base64_encode("Vista/Estadisticas/EstadisticasMAS.php") ?>?&e=" + edition;
                $("#result").load(path);
            } else {
                $("#result").html("");
            }
        });
    });
</script>