<?php

$val = $_GET["e"];
$facturaF=new Factura();
$Tpor = $facturaF -> consultarsemana();
$TporS = $facturaF -> consultarmes();
$TporD = $facturaF -> consultaraño();

$datos = "['Dia', 'Cantidad'], ";
    foreach ($Tpor as $l){
        $datos .= "['" . $l[0]  . " ', " . $l[1] . "], ";
    }
    $datos = substr($datos, 0, strlen($datos) - 1);

$datos2 = "['Mes', 'Cantidad'], ";
    foreach ($TporS as $l){
        $datos2 .= "['" . $l[0]  . " ', " . $l[1] . "], ";
    }
    $datos2 = substr($datos2, 0, strlen($datos2) - 1); 

$datos3 = "['Mes', 'Cantidad'], ";
    foreach ($TporD as $l){
        $datos3 .= "['" . $l[0]  . " ', " . $l[1] . "], ";
    }
    $datos3 = substr($datos3, 0, strlen($datos3) - 10); 

if($val==3){
    ?>
    
    <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
            <?php echo $datos ?>
            ]);

        var options = {
          width: 900,
          legend: { position: 'none' },
          chart: {
            title: 'INGRESOS SEMANA',
            subtitle: 'EL siguiente grafico mostrara los ingresos de los ultimos 7 dias:' },
          axes: {
            x: {
              0: { side: 'top', label: ''} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
    </script>
  </head>
  <body>
    <div id="top_x_div" style="width: 800px; height: 600px;"></div>
  </body>


   


<?php
}else if($val==2){
    ?>
    <head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
            <?php echo $datos2 ?>
            ]);

        var options = {
          width: 900,
          legend: { position: 'none' },
          chart: {
            title: 'INGRESOS MES',
            subtitle: 'EL siguiente grafico mostrara los ingresos de los ultimos 6 Meses:' },
          axes: {
            x: {
              0: { side: 'top', label: ''} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
    </script>
  </head>
  <body>
    <div id="top_x_div" style="width: 800px; height: 600px;"></div>
  </body>


     <?php  
}else{
    
?>

<head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['bar']});
      google.charts.setOnLoadCallback(drawStuff);

      function drawStuff() {
        var data = new google.visualization.arrayToDataTable([
            <?php echo $datos3 ?>
            ]);

        var options = {
          width: 900,
          legend: { position: 'none' },
          chart: {
            title: 'INGRESOS AÑO',
            subtitle: 'EL siguiente grafico mostrara los ingresos del ultimo año:' },
          axes: {
            x: {
              0: { side: 'top', label: ''} // Top x-axis.
            }
          },
          bar: { groupWidth: "90%" }
        };

        var chart = new google.charts.Bar(document.getElementById('top_x_div'));
        // Convert the Classic options to Material options.
        chart.draw(data, google.charts.Bar.convertOptions(options));
      };
    </script>
  </head>
  <body>
    <div id="top_x_div" style="width: 800px; height: 600px;"></div>
  </body>


<?php  
}
    
?>
