<?php

$carrito = $_SESSION["carrito"]; 
$carrito = unserialize($carrito); 
$arrayProductos = $carrito->getArrayProductos();

date_default_timezone_set('America/Bogota');
$fecha = date("Y-m-d H:i:s");

$idCliente = $_SESSION["id"];


$factura = new Factura("", $fecha, $idCliente);
$factura->crearFactura();
$ultimaFactura = $factura->facturas();
$idFactura = $ultimaFactura->getIdFactura();
 

$infoLog = "factura:".$idFactura;
date_default_timezone_set('America/Bogota');
$date = date('Y-m-d');
$hora = date('H:i:s');
$log = new Log("", 'Compra', $infoLog, $date, $hora, "Cliente", $_SESSION["id"]);
$log->insertarLog();


foreach ($arrayProductos as $productoActual) {
    $productoFactura = new ProductoFactura("", $productoActual->getidProducto(), $idFactura, $productoActual->getCantidad(), $productoActual->getPrecio());
    $productoFactura->crearProductoFactura();
    $productoBodega = new Producto($productoActual->getidProducto());
    $productoBodega->traerInfo();
    $nuevaCantidad = $productoBodega->getCantidad() - $productoActual->getCantidad();
    $productoBodega->setCantidad($nuevaCantidad);
    $productoBodega->actualizarCantidad();
}


$factura->setValor($carrito->precioTotal());
$factura->setIdFactura($idFactura);
$factura->actualizarValor();


$carritoNuevo = new Carrito();
$_SESSION["carrito"] = serialize($carritoNuevo);

$array = array(
    "estado" => $idCliente,
    "facturaID" => $factura->getIdFactura(),
    "carrito" => $carrito->precioTotal(),
    "factura" => $factura->getValor(),
);
$objJSON = json_encode($array);
echo $objJSON;
