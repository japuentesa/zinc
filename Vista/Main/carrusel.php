<div class="container col-md-12 " style="background-color: #003333;">
  <div class="row">
    <div class="col-xl-9 col-lg-9 col-md-12 col-12 p-3">
      <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
          <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner">
          <div class="carousel-item active">
            <img src="Vista/Img/Carrusel1.jpg" class="d-block w-100" white="100%">
            <div class="carousel-caption d-none d-md-block">
              <h5></h5>
              <p></p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="Vista/Img/carrusel2.jpg" class="d-block w-100" white="100%">
            <div class="carousel-caption d-none d-md-block">
              <h5></h5>
              <p></p>
            </div>
          </div>
          <div class="carousel-item">
            <img src="Vista/Img/carrusel3.jpg" class="d-block w-100">
            <div class="carousel-caption d-none d-md-block">
              <h5></h5>
              <p></p>
            </div>
          </div>
        </div>
        <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
          <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
          <span class="sr-only">Next</span>
        </a>
      </div>
    </div>
    <div class="col-xl-3 col-lg-3 col-md-12 col-12 p-3 d-lg-block d-sm-none d-md-none d-none ">
      <div>
        <img src="Vista/Img/SSS1.jpg" size="10px">
      </div>
      <div class="mt-xl-5 mt-lg-5 mt-md-2">
        <img src="Vista/Img/SSS2.jpg" alt="">
      </div>
    </div>
  </div>
</div>