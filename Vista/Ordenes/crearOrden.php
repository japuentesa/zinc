<?php

$infoLog = "";
$band = 0;
date_default_timezone_set('America/Bogota');
$datee = date('Y-m-d');
if (isset($_POST["Crear"])) {


    $producto = new ProveedorProducto("", $_POST["Id_Producto"], $_POST["Id_Proveedor"], $_POST["Cantidad"], "", $datee);


    $producto->insertar();
    $band = 2;
    $producto->traerInfo();
    $infoLog = "id_Orden:" . $producto->getIdProveedorProducto();
    date_default_timezone_set('America/Bogota');
    $date = date('Y-m-d');
    $hora = date('H:i:s');
    $log = new Log("", 'Crear Orden', $infoLog, $date, $hora, "Administrador", $_SESSION["id"]);
    $log->insertarLog();
}

?>


<div class="container">
    <div class="row ">
        <div class="col-12">
            <div class="card text-center">
                <div class="card-header bg-dark text-white">
                    <h4>Crear Orden</h4>
                </div>
                <div class="card-body" style="background-color: #9c9c9c;">
                    <div class="row  p-3">
                        <div class="col-xl-4 col-lg-3 col-md-12 col-sm-12 col-12">
                            <img src="https://icons.iconarchive.com/icons/icons-land/vista-people/256/Occupations-Technical-Support-Representative-Male-Light-icon.png" width="100%" class="img-thumbnail">
                        </div>
                        <div class="col-xl-8 col-lg-9 col-md-12 col-sm-12 col-12">
                            <form action="index.php?pid= <?php echo base64_encode("Vista/Ordenes/crearOrden.php") ?>" method="POST" enctype="multipart/form-data">
                            <div class="form-group">
                                    <div class="input-group-prepend p-4">
                                        <label class="input-group-text">Producto</label>
                                    </div>
                                    <select class="custom-select" name="Id_Producto" id="Id_Producto">
                                        <?php
                                        $producto = new Producto();
                                        $listaProducto = $producto->listarProductosO();
                                        foreach ($listaProducto as $productoActual) {
                                            echo "<option value='" . $productoActual->getIdProducto() . "'> " . $productoActual->getNombre() . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <div class="input-group-prepend p-4">
                                        <label class="input-group-text">Proveedor</label>
                                    </div>
                                    <select class="custom-select" name="Id_Proveedor" id="Id_Proveedor">
                                        <?php
                                        $proveedor = new Proveedor();
                                        $listaProveedor = $proveedor->listarProveedor();
                                        foreach ($listaProveedor  as $proveedorActual) {
                                            echo "<option value='" . $proveedorActual->getIdProveedor() . "'> " . $proveedorActual->getNombre() . "</option>";
                                        }
                                        ?>
                                    </select>
                                </div>

                                    <div class="form-group">
                                    <div class="input-group-prepend p-4">
                                        <label class="input-group-text" for="Cantidad">Cantidad</label>
                                    </div>
                                        <input type="number" class="form-control" name="Cantidad" id="Cantidad" required>
                                    </div>

                                    <div class="form-group text-center">
                                        <input name="Crear" type="submit" class="btn btn-outline-dark" value="Crear">
                                    </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($band == 2) {
    echo "<div class='modal fade' id='mostrarmodal' tabindex='-1' role='dialog' aria-labelledby='exampleModalLabel' aria-hidden='true'>";
    echo "<div class='modal-dialog '>";
    echo "<div class='modal-content '>";
    echo "<div class='alert alert-success m-0 d-flex justify-content-between'>";
    echo "<div></div>";
    echo "<h5 class='modal-title' id='exampleModalLabel'>Orden creada correctamente</h5>";
    echo "<button type='button' class='close' data-dismiss='modal' aria-label='Close'>";
    echo "<span aria-hidden='true'>&times;</span>";
    echo "</button>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
    echo "</div>";
} ?>