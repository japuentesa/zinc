<?php
$proveedor = new Proveedor($_SESSION["id"]);
$proveedor->traerInfo();
$log = new Log();
$fecha = $log->ultimaSesion("Proveedor", $_SESSION["id"]);
$info ="eyyy";
if (count($fecha) == 1) {
    $info = $fecha[0]->getFecha() . " " . $fecha[0]->getHora();
} else if(count($fecha) > 1){
    $valor = count($fecha) - 2;
    $info = $fecha[$valor]->getFecha() . " " . $fecha[$valor]->getHora();
}

?>

<div class="container-fluid">
    <div class="container pt-3 col-12" style="background-color: #003333;">
        <div class="card text-center " style="background-color: #222033;">
            <div class="card-header">
                <font face="Arial" Color="Orange">
                    <h5>¡Bienvenido!</h5>
            </div>
            <div class="card-body pb-0">
                <div class="row">
                    <div class="col-xl-6 col-lg-6 col-md-6 col-sm-12 col-12 border-right">
                        <div class="card text-center  border-0" style="background-color: #9c9c9c;">
                            <div class="card-body p-0">
                                <div class="row p-3">
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 text-center">
                                    <img src="<?php echo ($proveedor->getFoto() != "" ? $proveedor->getFoto() : "https://upload.wikimedia.org/wikipedia/commons/e/e4/Elliot_Grieveson.png") ?>" width="50%" class="img-thumbnail">
                                    </div>
                                    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 pt-4 px-5">
                                        <table class="table">
                                            <tbody>
                                                <tr>
                                                    <td>Nombre:</td>
                                                    <td><?php echo $proveedor->getNombre() ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Correo:</td>
                                                    <td><?php echo $proveedor->getCorreo() ?></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div class="col-xl-6 col-lg-6 col-md-6 d-md-block d-sm-none d-none">
                        <div>
                            <div class="container pt-6 mt-4 col-12" style="background-color: #003333;">
                                <img src="Vista/Img/carrusel14.jpg">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card-footer text-muted">
                <?php echo ($info!=""?"Ultima Sesion: ".$info:"") ?>
            </div>
        </div>
    </div>
</div>