<?php

session_start();
require_once "Negocio/Administrador.php";
require_once "Negocio/Cliente.php";
require_once "Negocio/Proveedor.php";
require_once "Negocio/Producto.php";
require_once "Negocio/Carrito.php";
require_once "Negocio/Factura.php";
require_once "Negocio/ProductoFactura.php";
require_once "Negocio/Log.php";
require_once "Negocio/ProveedorProducto.php";


$id = $_GET["idFactura"];
$factura = new Factura($id);
$factura->traerInfo();
$fechaHora = explode(" ", $factura->getFecha());
$cliente = new Cliente($factura->getIdCliente());
$cliente->traerInfo();
$productoFactura = new ProductoFactura("", "", $factura->getIdFactura());
$arrayfacturaProductos = $productoFactura->traerfacturaProducto();
?>
<?php

require_once "fpdf/fpdf.php";

$pdf = new FPDF();
$pdf -> AddPage('portrait','letter');

class pdf extends FPDF{

    public function header()
    {
        $this->SetFont('Arial','B',10);
        $this -> SetTextColor(64,10,0);
        $this->Cell(0,5,'Reportes ONIX',0,0,'C');
        $this->Image('on.jpg',170,5,37,20,'jpg');

    }
    public function footer()
    {
        $this->SetFont('Arial','B',10);
        $this->SetY(-15);
        $this->Write(5,' Colombia, Bogota D.C.');
        $this->SetX(-25);
        $this->AliasNbPages();
        $this->Write(5,$this -> PageNo().'/{nb}');

    }


}

$pdf = new pdf();
$pdf -> AddPage('portrait','letter');


$pdf -> ln(25);


$pdf -> SetFont('Arial','B',14);
$pdf -> SetTextColor(16,84,94);
$pdf -> Cell(0 ,10,'DETALLE DE FACTURA',0,0,'C'); 
$pdf -> ln(15);
$pdf -> SetLineWidth(1);
$pdf ->SetFillColor(80,80,80);
$pdf -> SetTextColor(175,175,175);
$pdf ->SetDrawColor(255,255,255);
$pdf -> SetFont('Arial','B',20);
$pdf -> Cell(0 ,15,'Informacion',1,0,'C',1); 
$pdf -> ln(15);
$pdf -> SetLineWidth(1);
$pdf ->SetFillColor(240,240,240);
$pdf -> SetTextColor(120,124,124);
$pdf ->SetDrawColor(255,255,255);
 
$pdf -> SetFont('Arial','',14);

$pdf -> Cell(100 ,10,'  Factura #:',1,0,'',1);
$pdf -> Cell(96 ,10,$factura->getIdFactura(),1,0,'R',1);
$pdf -> ln();
$pdf -> Cell(100 ,10,'  Fecha:',1,0,'',1); 
$pdf -> Cell(96 ,10, $fechaHora[0],1,0,'R',1);
$pdf -> ln();
$pdf -> Cell(100 ,10,'  Hora:',1,0,'',1); 
$pdf -> Cell(96 ,10,$fechaHora[1],1,0,'R',1);
$pdf -> ln();
$pdf -> Cell(100 ,10,'  Cliente:',1,0,'',1); 
$pdf -> Cell(96 ,10,$cliente->getNombre(),1,0,'R',1);
$pdf -> ln();
$pdf -> Cell(100 ,10,'  Correo:',1,0,'',1); 
$pdf -> Cell(96 ,10,$cliente->getCorreo(),1,0,'R',1);
$pdf -> ln(15);
$pdf -> ln(15);
$pdf -> SetLineWidth(1);
$pdf ->SetFillColor(80,80,80);
$pdf -> SetTextColor(175,175,175);
$pdf ->SetDrawColor(255,255,255);
$pdf -> SetFont('Arial','B',20);
$pdf -> Cell(0 ,15,'Productos',1,0,'C',1); 
$pdf -> ln(15);


$pdf -> SetTextColor(0,0,0);
$pdf ->SetFillColor(240,240,240);
$pdf -> SetTextColor(40,40,40);
$pdf -> SetFont('Arial','B',12);

$pdf -> Cell(30,10,'IdProducto',1,0,'C',false);  
$pdf -> Cell(56,10,'Nombre',1,0,'C',false);  
$pdf -> Cell(30,10,'Precio',1,0,'C',false);  
$pdf -> Cell(30,10,'Cantidad ',1,0,'C',false);  
$pdf -> Cell(50,10,'Subtotal',1,0,'C',false); 
$pdf -> SetDrawColor(61,174,233);
$pdf -> SetLineWidth(1);
$pdf -> Line(10,160,205,160); 

foreach ($arrayfacturaProductos  as $actual) {
    $producto = new Producto($actual->getidProducto());
    $producto->traerInfo();
    $pdf -> ln(11);
$pdf -> SetLineWidth(1);
$pdf ->SetDrawColor(255,255,255);
$pdf ->SetFillColor(240,240,240);
$pdf -> SetFont('Arial','',12);
$pdf -> Cell(30,10,$actual->getidProducto(),1,0,'C',false);  
$pdf -> Cell(56,10,$producto->getNombre() ,1,0,'C',false);  
$pdf -> Cell(30,10,$producto->getPrecio(),1,0,'C',false);  
$pdf -> Cell(30,10,$actual->getCantidad(),1,0,'C',false); 
$subtotal = $actual->getCantidad() * $actual->getPrecio(); 
$pdf -> Cell(50,10,$subtotal ,1,0,'C',false);
}
$pdf -> ln();
$pdf -> Cell(100 ,10,' Precio Total:',1,0,'R',1); 
$pdf -> Cell(96 ,10,$factura->getValor()    ,1,0,'C',1);
$pdf -> OutPut();





?>